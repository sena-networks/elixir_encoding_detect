import Config

if Mix.env == :dev or Mix.env == :test do
  config :mix_test_watch,
    extra_extensions: [".rs"]
end