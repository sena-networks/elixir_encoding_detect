defmodule ElixirEncodingDetectTest do
	use ExUnit.Case

	test "test" do
		{:ok, sjis} = File.read(Application.app_dir(:elixir_encoding_detect, "priv/test_data/sjis"))

		{:ok, eucjp} = File.read(Application.app_dir(:elixir_encoding_detect, "priv/test_data/eucjp"))

		{:ok, iso2022jp} = File.read(Application.app_dir(:elixir_encoding_detect, "priv/test_data/iso2022jp"))

		{:ok, utf8} = File.read(Application.app_dir(:elixir_encoding_detect, "priv/test_data/utf8"))

		{:ok, unknown} = File.read(Application.app_dir(:elixir_encoding_detect, "priv/test_data/unknown"))

		assert {:ok, "SHIFT_JIS"} == UchardetModule.detect_encoding_name(sjis)

		assert {:ok, "EUC-JP"} == UchardetModule.detect_encoding_name(eucjp)

		assert {:ok, "ISO-2022-JP"} == UchardetModule.detect_encoding_name(iso2022jp)

		assert {:ok, "UTF-8"} == UchardetModule.detect_encoding_name(utf8)

		assert {:error, "uchardet::ErrorKind::UnrecognizableCharset"} == UchardetModule.detect_encoding_name(unknown)
	end
end
