defmodule UchardetModule do
	@moduledoc """
	判別に使用したいbinaryを入力すると{:ok, "エンコーディング名"}か{:error, "エラー内容"}を返す。
	エンコーディング名は
	https://www.freedesktop.org/wiki/Software/uchardet/
	を参照。
	"""

	use Rustler, otp_app: :elixir_encoding_detect, crate: :uchardetmodule

	@doc """
	{:ok, file} = File.read("priv/textfile")

	{:ok, "encoding-name"} = UchardetModule.detect_encoding_name(file)
	"""
	@spec detect_encoding_name(binary()) :: {:ok, binary()} | {:error, binary()}
	def detect_encoding_name(_binary), do: exit(:nif_not_loaded)
end