# [rust-uchardet](https://github.com/emk/rust-uchardet) elixir port

[rust-uchardet](https://github.com/emk/rust-uchardet)のElixir Portです。

# install

```elixir
def deps do
  [
    {:elixir_encoding_detect, "~> 0.1.0"}
  ]
end
```

# 使い方

```elixir
{:ok, file} = File.read("priv/textfile")

{:ok, "encoding-name"} = UchardetModule.detect_encoding_name(file)
```
