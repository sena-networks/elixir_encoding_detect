#[derive(rustler::NifTaggedEnum)]
enum R {
	Ok(String),
}

fn uchardet_error_to_string(e: uchardet::Error) -> String {
	match e.kind() {
		uchardet::ErrorKind::Msg(m) => format!("uchardet::ErrorKind::Msg({m})").to_string(),
		uchardet::ErrorKind::UnrecognizableCharset => "uchardet::ErrorKind::UnrecognizableCharset".to_string(),
		uchardet::ErrorKind::OutOfMemory => "uchardet::ErrorKind::OutOfMemory".to_string(),
		uchardet::ErrorKind::Other(i) => format!("uchardet::ErrorKind::Other({i})").to_string(),
	}
}

#[rustler::nif]
fn detect_encoding_name(data: rustler::types::binary::Binary) -> rustler::NifResult<R> {
	match uchardet::detect_encoding_name(data.as_slice()) {
		Ok(enc) => Ok(R::Ok(enc)),
		Err(e) => Err(rustler::Error::Term(Box::new(uchardet_error_to_string(e)))),
	}
}

rustler::init!("Elixir.UchardetModule", [detect_encoding_name]);