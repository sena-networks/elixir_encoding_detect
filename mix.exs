defmodule ElixirEncodingDetect.MixProject do
  use Mix.Project

  def project do
    [
      app: :elixir_encoding_detect,
      version: "0.1.0",
      elixir: "~> 1.14",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      name: "ElixirEncodingDetect",
      package: package(),
      description: description(),
      source_url: "https://gitlab.com/sena-networks/elixir_encoding_detect"
    ]
  end

  defp description() do
    "[rust-uchardet](https://github.com/emk/rust-uchardet) elixir port"
  end

  defp package() do
    [
      name: "elixir_encoding_detect",
      files: ~w(lib native priv .formatter.exs mix.exs README.md test),
      licenses: ["MIT"],
      links: %{"Bitbucket" => "https://gitlab.com/sena-networks/elixir_encoding_detect"}
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:mix_test_watch, "~> 1.1", only: [:dev, :test], runtime: false},
      {:ex_doc, "~> 0.14", only: :dev, runtime: false},
      {:rustler, "~> 0.30.0"},
    ]
  end
end
